Stopwatch
=========

A simple stopwatch class.

    import stopwatch.py
    import time

    sw = Stopwatch() # The stopwatch is not started on initialization.

    sw.start()
    time.sleep(10)
    sw.stop()

    print sw.elapsed() # Elapsed time can be printed without stopping the stopwatch.
