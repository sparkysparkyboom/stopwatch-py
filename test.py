import stopwatch
import time

sw = stopwatch.Stopwatch()
sw.start()
time.sleep(3)
print sw.stopped
print sw.elapsed()
sw.stop()
print sw.stopped

sw.start()
time.sleep(3)
print sw.stopped
print sw.elapsed()

time.sleep(3)
print sw.stopped
print sw.elapsed()
sw.stop()
print sw.stopped
