import time

class Stopwatch:
	start_time = 0.0
	end_time = 0.0
	stopped = True
	
	def __init__(self):
		# Add what to do on initialization later.
		pass

	def start(self):
		# Starts the stopwatch

		#region Start again
			# Checks if the stopwatch has been started and stopped.
		if ((self.start_time != 0.0) and (self.stopped == True)):
			print 'start_time ' + str (self.start_time)
			self.start_time -= (time.time() - self.end_time) # Subtracts the amount of time for which it has been stopped from the start_time
			print 'time ' + str(time.time())
			print 'start_time ' + str(self.start_time)
			self.end_time  = 0.0
			self.stopped = False
		#endregion Start again


		#region Started already
			# Exits the function if the stopwatch has already been started
		elif (self.stopped ==  False):
			return None
		#endregion Started already

		
		else: # Started for the first time.
			self.start_time = time.time()
			self.end_time = 0.0
			self.stopped = False

	def stop(self):
		self.end_time = time.time()
		self.stopped = True
	
	def elapsed(self):
		if (self.stopped == True):
			return (self.end_time - self.start_time)

		else:
			return (time.time() - self.start_time)
	
	def reset(self):
                self.start_time = time.time()
                self.end_time = 0.0 
                self.stopped = True
	
	def restart(self):
		self.reset()
		self.start()
